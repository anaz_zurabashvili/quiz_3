package com.example.quiz_3.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.quiz_3.databinding.AddUserLayoutBinding


class AddUserActivity : AppCompatActivity() {
    private lateinit var binding: AddUserLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AddUserLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.saveBtn.setOnClickListener(saveUser)
    }

    private val saveUser = View.OnClickListener {
        val intent = intent
        if (isAllFieldsValid()) {
            val user = User(
                firstName = binding.tvFirstName.text.toString(),
                lastName = binding.tvFirstName.text.toString(),
                emailAddress = binding.tvFirstName.text.toString(),
            )
            val intent = intent
            intent.putExtra("user", user)
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    private fun isAllFieldsValid(): Boolean = !(binding.tvEmailAddress.text.isNullOrBlank() ||
            binding.tvFirstName.text.isNullOrBlank() ||
            binding.tvLastName.text.isNullOrBlank())

}