package com.example.quiz_3.activities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class User(val firstName: String, val lastName: String, val emailAddress: String): Parcelable

