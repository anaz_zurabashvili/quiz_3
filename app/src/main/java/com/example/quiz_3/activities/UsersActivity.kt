package com.example.quiz_3.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz_3.databinding.UsersActivityBinding

class UsersActivity : AppCompatActivity() {
    val users = ArrayList<User>()
    private val position = 0
    private lateinit var binding: UsersActivityBinding
    private lateinit var adapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = UsersActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun initRecycleView() {
        setData()
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UsersAdapter(users)
        binding.recyclerView.adapter = adapter
    }

    private fun init() {
        initRecycleView()
//        binding.ivBtnImage.setOnClickListener(addUserBtn)
    }

    private val addUserBtn = View.OnClickListener {
        startForResult.launch(Intent(this, AddUserActivity::class.java))
    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) {
                for
                it.data?.extras?.getParcelable<User>("user")
//                if (user != null){
//                    addUser(user)
//                }
            }
        }

    private fun setData() {
        users.add(User("User 1", "Content 1", "emailAddress"))
        users.add(User("User 2", "Content 2", "emailAddress"))
        users.add(User("User 3", "Content 3", "emailAddress"))
        users.add(User("User 4", "Content 4", "emailAddress"))
        users.add(User("User 5", "Content 5", "emailAddress"))
        users.add(User("User 6", "Content 6", "emailAddress"))
    }

    private fun addUser(user: User?) {
        user?.let { users.add(it) }
    }

}

