package com.example.quiz_3.activities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_3.databinding.UserLayoutBinding


class UsersAdapter(private val items: MutableList<User>) :
    RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    //    აბრუნებს რესაიქლ ვიუს ვიუ ჰოლდერს
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(UserLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    //იმდენჯერ რამდენი აიტემიც არსებობს გამოიძახება, ინფორმაციის დასეტვა აქ ხდება
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = items.size

    //    ფოზიშენის მიხედვით რა ტიპის აითემის დასეტვას აპირებ რესაიქლ ვიუში
    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    inner class ViewHolder(val binding: UserLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var model: User

        fun onBind() {
            model = items[adapterPosition]
            binding.tvFirstName.text =  model.firstName
            binding.tvLastName.text =  model.lastName
            binding.tvEmailAddress.text =  model.emailAddress
        }
    }
}